package com.epam.CarRentalSystem.dao;

import com.epam.CarRentalSystem.domain.Vehicle;

import java.util.List;

public interface VehicleDAO {
    void deleteByVehicleId(Integer vehicleId);
    Vehicle findByVehicleId(Integer vehicleId);
    List<Vehicle> findAll();

}
