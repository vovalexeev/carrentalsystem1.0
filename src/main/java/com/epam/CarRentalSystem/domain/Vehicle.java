package com.epam.CarRentalSystem.domain;

import java.util.Date;

public class Vehicle {
    /*//Идеи Жени
    private String Model;
    private String color;
    private String numberCar;
    private int year;
    private double cost; //int maybe */
    //Идеи Вовы
    private Integer vehicleID;
    private String brand;
    private String model;
    private String type; //enum maybe
    private String regNumber;
    private double hourPrice;
    private boolean isAvailable; //на ходу или нет
    private boolean isRented; //арендована ли в данный момент
    private Date lastServiceDate;
    private String color;
    private int numberOfSeats;
    private String gearbox; //enum maybe
    private double fuelConsumption;
    private double power;

    public Vehicle() {
    }

    public Vehicle(Integer vehicleID, String brand, String model, String type, String regNumber,
                   double hourPrice, boolean isAvailable, boolean isRented, Date lastServiceDate,
                   String color, int numberOfSeats, String gearbox, double fuelConsumption, double power) {
        this.vehicleID = vehicleID;
        this.brand = brand;
        this.model = model;
        this.type = type;
        this.regNumber = regNumber;
        this.hourPrice = hourPrice;
        this.isAvailable = isAvailable;
        this.isRented = isRented;
        this.lastServiceDate = lastServiceDate;
        this.color = color;
        this.numberOfSeats = numberOfSeats;
        this.gearbox = gearbox;
        this.fuelConsumption = fuelConsumption;
        this.power = power;
    }

    public Integer getVehicleID() {
        return vehicleID;
    }

    public void setVehicleID(Integer vehicleID) {
        this.vehicleID = vehicleID;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public double getHourPrice() {
        return hourPrice;
    }

    public void setHourPrice(double hourPrice) {
        this.hourPrice = hourPrice;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean isRented() {
        return isRented;
    }

    public void setRented(boolean rented) {
        isRented = rented;
    }

    public Date getLastServiceDate() {
        return lastServiceDate;
    }

    public void setLastServiceDate(Date lastServiceDate) {
        this.lastServiceDate = lastServiceDate;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public String getGearbox() {
        return gearbox;
    }

    public void setGearbox(String gearbox) {
        this.gearbox = gearbox;
    }

    public double getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public double getPower() {
        return power;
    }

    public void setPower(double power) {
        this.power = power;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "vehicleId=" + vehicleID +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", type='" + type + '\'' +
                ", regNumber='" + regNumber + '\'' +
                ", hourPrice=" + hourPrice +
                ", isAvailable=" + isAvailable +
                ", isRented=" + isRented +
                ", lastServiceDate=" + lastServiceDate +
                ", numberOfSeats=" + numberOfSeats +
                ", gearbox='" + gearbox + '\'' +
                ", fuelConsumption=" + fuelConsumption +
                ", power=" + power +
                '}';
    }
}
