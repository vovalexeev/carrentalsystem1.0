package com.epam.CarRentalSystem.domain;

import java.util.Date;
import java.util.List;

public class Booking {
    /*//Идеи Жени
        private UserForCRUDApp userForCRUDApp;
        private List<Vehicle> cars;
        private int amountHours;*/
    //Идеи Вовы
    private Integer vehicleID;
    private Integer bookingID;
    private double totalCost;
    private Date startDate;
    private Date returnDate;
    private double penaltyFee;



    public Integer getVehicleID() {
        return vehicleID;
    }

    public void setVehicleID(Integer vehicleID) {
        this.vehicleID = vehicleID;
    }

    public Integer getBookingID() {
        return bookingID;
    }

    public void setBookingID(Integer bookingID) {
        this.bookingID = bookingID;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public double getPenaltyFee() {
        return penaltyFee;
    }

    public void setPenaltyFee(double penaltyFee) {
        this.penaltyFee = penaltyFee;
    }
}
